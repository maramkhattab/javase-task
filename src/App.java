import java.util.*;
import java.util.stream.Collectors;
public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Employee> employees=Arrays.asList(new Employee("ASE","Maram",01112231553),
				new Employee("ASE","Mariam",01223344442), 
				new Employee("SE","Yomna",0124435535));
		Map<String, List<Employee>> employeesGrouped =
			    employees.stream().collect(Collectors.groupingBy(e -> e.getTitle()));
		employeesGrouped.forEach((key,value)->{
			System.out.print("Title: "+key+" Count: "+value.size()+"\n");
			value.forEach(e->System.out.print("Name: "+e.getName()+" Number: "+ e.getNumber()+"\n"));
			
		});

	}

}
