
public class Employee {
	private String title;
	private String name;
	private long number;
	
	public Employee(String title, String name, long number){
		this.title=title;
		this.name=name;
		this.number=number;
	}
	String getTitle() {
		return this.title;
	}
	String getName() {
		return this.name;
	}
	long getNumber() {
		return this.number;
	}

}
